<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{	
	protected $table = 'users';
    protected $fillable = ['name', 'email', 'password', 'photo'];
    protected $hidden = ['password', 'remember_token'];

    public function status()
    {
        return $this->hasMany('App\Status');
    }
}
