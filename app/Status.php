<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Status extends Model
{	
	protected $table = 'statuses';
    protected $fillable = ['description', 'user_id'];
    protected $with = ['user'];

    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
