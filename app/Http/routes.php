<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::auth();

Route::group(['middleware' => 'auth'], function () {
	Route::get('/', 'HomeController@index');

	Route::get('profile', 'UserController@index');

	Route::post('tweet', 'HomeController@insert');
	Route::post('profile/update', 'UserController@update');
	Route::post('profile/photoUpdate', 'UserController@photoUpdate');
});
