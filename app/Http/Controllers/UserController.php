<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\User as User;

class UserController extends Controller
{
    public function index()
    {
        return view('profile');
    }

    public function update(Request $request)
    {	
    	$add_requirement = $request->user()->email == $request->email ? '' : '|unique:users';

    	$this->validate($request, [
	        'name' => 'required|min:3',
	        'email' => 'required|email'.$add_requirement,
	        'password' => 'min:6'
	    ]);

	    $request->has('password') ? $request->merge(['password' => bcrypt($request->password)]) : $request->offsetUnset('password');

  		$user = User::where('id', $request->user()->id)->firstOrFail()
  			->fill($request->all());

  		if ($user->save()) {
  			\Session::flash('success_message', 'Saved.');
  		} else {
  			\Session::flash('error_message', 'Failed.');
  		}
		
	    return redirect()->back();
    }

    public function photoUpdate(Request $request)
    {	
    	$this->validate($request, [
	        'photo' => 'required'
	    ]);

    	$file = $request->file('photo');
    	$name = md5($request->user()->id).'.'.$file->getClientOriginalExtension();
    	$destinationPath = 'images/avatars';
      	$is_upload = $file->move($destinationPath, $name);

      	if ($is_upload) {
      		$user = User::where('id', $request->user()->id)->firstOrFail();
      		$user->photo = $name;

			if ($user->save()) {
				\Session::flash('success_message', 'Saved.');
			} else {
				\Session::flash('error_message', 'Failed.');
			}
      	} else {
      		\Session::flash('error_message', 'Upload Failed.');
      	}

	    return redirect()->back();
    }
}
