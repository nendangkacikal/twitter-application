<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;
use App\Status as Status;

class HomeController extends Controller
{
    public function index()
    {	
    	$data['statuses'] = Status::orderBy('created_at', 'desc')
               ->get();

        return view('home', $data);
    }

    public function insert(Request $request)
    {	
    	$this->validate($request, [
	        'description' => 'required|min:3'
	    ]);

	    $request->request->add(['user_id' => $request->user()->id]);

  		$status = new Status;

  		$status->fill($request->all());

  		if ($status->save()) {
  			\Session::flash('success_message', 'Success.');
  		} else {
  			\Session::flash('error_message', 'Failed.');
  		}
		
	    return redirect()->back();
    }
}
