@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            @if(Session::has('success_message'))
                <p class="alert alert-success">{{ Session::get('success_message') }}</p>
            @endif

            @if(Session::has('error_message'))
                <p class="alert alert-danger">{{ Session::get('error_message') }}</p>
            @endif
        </div>
        <div class="col-md-8 col-md-offset-2">
            <form class="form-horizontal" role="form" method="POST" action="{{ url('/tweet') }}">
                {{ csrf_field() }}
                <div class="form-group{{ $errors->has('description') ? ' has-error' : '' }}">
                    <div class="col-md-12">
                        <label for="description">Name</label>
                        <input id="description" type="text" class="form-control" name="description" value="{{ old('description') }}">

                        @if ($errors->has('description'))
                            <span class="help-block">
                                <strong>{{ $errors->first('description') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-12">
                        <button type="submit" class="btn btn-primary pull-right">
                            Update
                        </button>
                    </div>
                </div>
            </form>
            <hr>
        </div>
        <div class="col-md-8 col-md-offset-2">
            @foreach ($statuses as $status)
                <div class="panel panel-default">
                    <div class="panel-body{{ $status->user->id == Auth::user()->id ? ' me' : '' }}">
                        <div class="media">
                            @if ($status->user->id != Auth::user()->id)
                                <div class="media-left">
                                    <img class="media-object img-circle" width="75" height="75" src="/images/avatars/{{ is_null($status->user->photo) ? 'default.jpg' : $status->user->photo }}" alt="user image">
                                </div>
                            @endif
                          
                            <div class="media-body">
                                <h4 class="media-heading">{{ $status->user->name }} <small>&middot; {{ $status->created_at->diffForHumans() }}</small></h4>
                                {{ $status->description }}
                            </div>

                            @if ($status->user->id == Auth::user()->id)
                                <div class="media-right">
                                    <img class="media-object img-circle" width="75" height="75" src="/images/avatars/{{ is_null($status->user->photo) ? 'default.jpg' : $status->user->photo }}" alt="user image">
                                </div>
                            @endif
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
</div>
@endsection
